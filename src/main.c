#include <stdio.h>
#include <endian.h>

#include "ppc-opcode.h"

int idump_opcode(FILE *f, uint32_t v, uint32_t addr){
 int i;
 i=0;
 for(i=0;opcodes[i].name != NULL;i++){
  if( (opcodes[i].mask & v) == opcodes[i].op ){
   if(opcodes[i].p(f, opcodes[i], v, addr)){
    return 1;
   }
   return 0;
  }
 }
 fprintf(f, ".int 0x%08x", v);
 return 0;
}

int main(int argc, char *argv[]){

 int i;
 uint32_t addr;
 uint32_t v;
 FILE *fout;
 FILE *fin;

 fin = stdin;
 fout = stdout;
 if(argc >= 2){
  fin = fopen(argv[1], "rb");
  if(fin == NULL){
   return 1;
  }
 }
 if(argc >= 3){
  fout = fopen(argv[2], "wb");
  if(fout == NULL){
   return 1;
  }
 }

 addr = 0;
 while(1){
  if(fread(&v, 4, 1, fin) != 1){
   return 0;
  }
  /* big endian */
  v = be32toh(v);
  fprintf(fout, "%08x:\t%08x\t", addr, v);
  if(idump_opcode(fout, v, addr)){
   fprintf(stderr, "error in idump_opcode\n");
   return 1;
  }
  fprintf(fout, "\n");
  addr+=4;
 }

 return 0;
}
