#include <stdio.h>
#include "ppc-opcode.h"

#define S_OPCD (31-5)
#define W_OPCD(_op) (((_op) & 0x3f) << S_OPCD)
#define MASK_OPCD W_OPCD(0x3f)
#define R_OPCD(_op) (((_op) & MASK_OPCD) >> S_OPCD)

#define S_XO (31-30)
#define W_XO(_op) (((_op) & 0x3ff) << S_XO)
#define MASK_XO W_XO(0x3ff)
#define MASK_OEXO W_XO(0x1ff)
#define R_XO(_op) (((_op) & MASK_XO) >> S_XO)
#define R_OEXO(_op) (((_op) & MASK_OEXO) >> S_XO)

#define S_OE (31-21)
#define W_OE(_v) (((_v) & 1) << S_OE)
#define MASK_OE W_OE(1)
#define R_OE(_op) (((_op) & MASK_OE) >> S_OE)

#define S_RC (31-31)
#define W_RC(_v)  (((_v) & 1) << S_RC)
#define MASK_RC W_RC(1)
#define R_RC(_op) (((_op) & MASK_RC) >> S_RC)

#define S_D (31-10)
#define W_D(_v) (((_v) & 0x1f) << S_D)
#define MASK_D W_D(0x1f)
#define R_D(_op) (((_op) & MASK_D) >> S_D)

#define S_A (31-15)
#define W_A(_v) (((_v) & 0x1f) << S_A)
#define MASK_A W_A(0x1f)
#define R_A(_op) (((_op) & MASK_A) >> S_A)

#define S_B (31-20)
#define W_B(_v) (((_v) & 0x1f) << S_B)
#define MASK_B W_B(0x1f)
#define R_B(_op) (((_op) & MASK_B) >> S_B)

#define S_SIMM (31-31)
#define W_SIMM(_v) (((_v) & 0xffff) << S_SIMM)
#define MASK_SIMM W_SIMM(0xffff)
#define R_SIMM(_op) (((_op) & MASK_SIMM) >> S_SIMM)

#define S_LI (31-29)
#define W_LI(_v) (((_v) & 0xffffff) << S_LI)
#define MASK_LI W_LI(0xffffff)
#define R_LI(_op) (((_op) & MASK_LI) >> S_LI)

#define S_AA (31-30)
#define W_AA(_v) (((_v) & 0x1) << S_AA)
#define MASK_AA W_AA(0x1)
#define R_AA(_op) (((_op) & MASK_AA) >> S_AA)

#define S_LK (31-31)
#define W_LK(_v) (((_v) & 0x1) << S_LK)
#define MASK_LK W_LK(0x1)
#define R_LK(_op) (((_op) & MASK_LK) >> S_LK)

#define S_BO (31-10)
#define W_BO(_v) (((_v) & 0x1f) << S_BO)
#define MASK_BO W_BO(0x1f)
#define R_BO(_op) (((_op) & MASK_BO) >> S_BO)

#define S_BI (31-15)
#define W_BI(_v) (((_v) & 0x1f) << S_BI)
#define MASK_BI W_BI(0x1f)
#define R_BI(_op) (((_op) & MASK_BI) >> S_BI)

#define S_BD (31-29)
#define W_BD(_v) (((_v) & 0x3fff) << S_BD)
#define MASK_BD W_BD(0x3fff)
#define R_BD(_op) (((_op) & MASK_BD) >> S_BD)

/* I-Form */
int p_i(FILE *f, opcode_t op, uint32_t v, uint32_t addr);
/* B-Form */
int p_b(FILE *f, opcode_t op, uint32_t v, uint32_t addr);
/* SC-Form */
int p_sc(FILE *f, opcode_t op, uint32_t v, uint32_t addr);
/* XO-Form */
int p_xo(FILE *f, opcode_t op, uint32_t v, uint32_t addr);

opcode_t opcodes[] = {
 /* I-Form */
 /* 0-5  6-29 30 31 */
 /* OPCD  LI  AA LK */
 {"b", W_OPCD(18),
  MASK_OPCD, T_I_FORM, p_i},
 /* B-Form */
 /* 0-5  6-10 11-15 16-29 30 31 */
 /* OPCD  BO    BI    BD  AA LK */
 {"bc", W_OPCD(16),
  MASK_OPCD, T_B_FORM, p_b},
 /* SC-Form */
 /* 0-5  6-10 11-15 16-29 30 31 */
 /* OPCD  0     0     0   1  0 */
 {"sc", W_OPCD(17) | W_BO(0) | W_BI(0) | W_BD(0) | W_AA(1) | W_LK(0),
  MASK_OPCD | MASK_BO | MASK_BI | MASK_BD | MASK_B | MASK_AA | MASK_LK,
  T_SC_FORM, p_sc},
 /* XO-Form */
 /*      0-5  6-10 11-15 16-20 21 22-30 31 */
 /* XO_1 OPCD  D     A     B   OE  XO   Rc */
 /* XO_2 OPCD  D     A     B   0   XO   Rc */
 /* XO_3 OPCD  D     A     0   OE  XO   Rc */
 /* XO_OE */
 {"add", W_OPCD(31) | W_XO(266),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"addc", W_OPCD(31) | W_XO(10),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"adde", W_OPCD(31) | W_XO(138),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"addme", W_OPCD(31) | W_XO(234),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"addze", W_OPCD(31) | W_XO(202) | W_B(0),
  MASK_OPCD | MASK_B | MASK_OEXO, T_XO_FORM, p_xo},
 {"divw", W_OPCD(31) | W_XO(491),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"divwu", W_OPCD(31) | W_XO(459),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"mulhw", W_OPCD(31) | W_XO(75),
  MASK_OPCD | MASK_OEXO | MASK_OE, T_XO_FORM, p_xo},
 {"mulhwu", W_OPCD(31) | W_XO(11),
  MASK_OPCD | MASK_OEXO | MASK_OE, T_XO_FORM, p_xo},
 {"mullw", W_OPCD(31) | W_XO(235),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"neg", W_OPCD(31) | W_XO(104) | W_B(0),
  MASK_OPCD | MASK_B | MASK_OEXO, T_XO_FORM, p_xo},
 {"subf", W_OPCD(31) | W_XO(40),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"subfc", W_OPCD(31) | W_XO(8),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"subfe", W_OPCD(31) | W_XO(136),
  MASK_OPCD | MASK_OEXO, T_XO_FORM, p_xo},
 {"subfme", W_OPCD(31) | W_XO(232) | W_B(0),
  MASK_OPCD | MASK_B | MASK_OEXO, T_XO_FORM, p_xo},
 {"subfze", W_OPCD(31) | W_XO(200) | W_B(0),
  MASK_OPCD | MASK_B | MASK_OEXO, T_XO_FORM, p_xo},

 {NULL}
};

int p_sc(FILE *f, opcode_t op, uint32_t v, uint32_t addr){
 fprintf(f, op.name);
 return 0;
}

int p_b(FILE *f, opcode_t op, uint32_t v, uint32_t addr){
 int32_t a;
 a = R_BD(v);
 a <<= 2;
 if(a & 0x8000){
  a |= 0xffff0000;
 }
 fprintf(f, op.name);
 if(R_LK(v)){
  fprintf(f, "l");
 }
 if(R_AA(v)){
  fprintf(f, "a");
 }else{
  a += addr;
 }
 fprintf(f, " 0x%x,%d,0x%x", R_BO(v), R_BI(v), a);
 return 0;
}

int p_i(FILE *f, opcode_t op, uint32_t v, uint32_t addr){
 int32_t a;
 a = R_LI(v);
 a <<= 2;
 if(a & 0x02000000){
  a |= 0xfc000000;
 }
 fprintf(f, op.name);
 if(R_LK(v)){
  fprintf(f, "l");
 }
 if(R_AA(v)){
  fprintf(f, "a");
 }else{
  a += addr;
 }
 fprintf(f, " 0x%x", a);
 return 0;
}

int p_xo(FILE *f, opcode_t op, uint32_t v, uint32_t addr){
 fprintf(f, op.name);
 if(R_OE(v)){
  fprintf(f, "o");
 }
 if(R_RC(v)){
  fprintf(f, ".");
 }
 fprintf(f, " r%d,r%d", R_D(v), R_A(v));
 if((op.mask & MASK_B) == 0){
  fprintf(f, ",r%d", R_B(v));
 }
 return 0;
}
