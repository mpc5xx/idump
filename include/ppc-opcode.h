#ifndef __PPC_OPCODE_H__
#define __PPC_OPCODE_H__

#include <stdint.h>

enum _form_t { T_I_FORM, T_B_FORM, T_SC_FORM, T_D_FORM,
 T_X_FORM, T_XL_FORM, T_XF_FORM, T_XO_FORM, T_M_FORM};
typedef enum _form_t form_t;

struct _opcode_t {
 char *name;
 uint32_t op;
 uint32_t mask;
 form_t form;
 int (*p)(FILE *, struct _opcode_t, uint32_t, uint32_t);
};
typedef struct _opcode_t opcode_t;

typedef int (*printop_t)(FILE *, opcode_t, uint32_t, uint32_t);

extern opcode_t opcodes[];

#endif /* __PPC_OPCODE_H__ */
