
prog=idump

objdir=obj
srcdir=src

src=main.c ppc-opcode.c
obj=$(addprefix $(objdir)/,$(patsubst %.c,%.o,$(src)))

CC=gcc
CFLAGS=-Iinclude
LDFLAGS=

all : $(prog)

$(prog) : $(objdir) $(obj)
	$(CC) -o $(prog) $(obj)

$(objdir)/%.o : $(srcdir)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(objdir) :
	mkdir -p $(objdir)

clean : 
	rm -rf $(objdir)
	rm -rf $(prog)
